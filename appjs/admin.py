from django.contrib import admin
from .models import Kegiatan,Prestasi,Organisasi

admin.site.register(Kegiatan)
admin.site.register(Prestasi)
admin.site.register(Organisasi)
# Register your models here.
