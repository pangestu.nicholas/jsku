from django.shortcuts import render,HttpResponse, redirect
from .models import Kegiatan,Prestasi,Organisasi
def home(request):
    
    list_orga=Organisasi.objects.all()
    list_pres=Prestasi.objects.all()
    list_keg=Kegiatan.objects.all()

    context = {
        'kegiatan':list_keg,
        'prestasi': list_pres,
        'organisasi':list_orga
    }

    return render(request,'home.html',context)