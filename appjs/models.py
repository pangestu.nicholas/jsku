from django.db import models

class Kegiatan(models.Model):
    pesan_kegiatan=models.CharField(max_length=300)

class Prestasi(models.Model):
    pesan_prestasi=models.CharField(max_length=250)

class Organisasi(models.Model):
    pesan_organisasi=models.CharField(max_length=200)
