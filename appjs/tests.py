from django.test import TestCase, Client
import time
from django.urls import resolve
from .views import home
from .models import Kegiatan,Prestasi,Organisasi
from selenium.webdriver.chrome.options import Options
import unittest
from selenium import webdriver
from django.test import LiveServerTestCase

class unitTest(TestCase):

    def test_url_is_existed(self):
        response=Client().get('/')
        print(response.status_code)
        self.assertEqual(response.status_code,200)

    def test_template_exist(self):
        response=Client().get('/')
        self.assertTemplateUsed(response,"home.html")
    
    def test_function_found(self):
        response=resolve('/')
        self.assertEqual(response.func,home)

    def test_create_model(self):
        peskeg="teskeg"
        keg=Kegiatan.objects.create(pesan_kegiatan=peskeg)
        jumlahKeg=Kegiatan.objects.all().count()
        self.assertEqual(jumlahKeg,1)

        pespre="tespre"
        pre=Prestasi.objects.create(pesan_prestasi=pespre)
        jumlahPre=Prestasi.objects.all().count()
        self.assertEqual(jumlahPre,1)

        pesorg="tesorg"
        org=Organisasi.objects.create(pesan_organisasi=pesorg)
        jumlahOrg=Organisasi.objects.all().count()
        self.assertEqual(jumlahOrg,1)

class FungtionalTes(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_navigate_site(self):
        self.browser.get("http://localhost:8000/")
         
            # "http://localhost:8000/"
        # self.assertRaises(AssertionError,)
        
        self.assertIn("Saat",self.browser.page_source)

        self.assertIn("Story 7",self.browser.page_source)

        self.browser.find_element_by_id("atribut").click()

        self.assertIn("Background", self.browser.page_source)
        time.sleep(3) # Let the user actually see something!
